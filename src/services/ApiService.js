import axios from "axios";
import { store } from "../store/store";
// import envi from '../config/dev.env';

const urllogin = user => {
  let headers = {"Content-Type":"application/json", "Access-Control-Allow-Origin":"*"};
  console.log("User en APISERVICE", user);
  return axios.post('http://localhost:8090/login',user);
};

const urlregister = user => {
    return axios.post(`${process.env.API_URL}/user/register`, user);
};

const allUser = () => {
  let headers = { Authorization: store.state.user_token };
  console.log("HEAD", headers);
  return axios.get(`${process.env.API_URL}/user/alluser`, { headers });
};

const updateUser = user => {
  let headers = { Authorization: store.state.user_token };
  return axios.put(`${process.env.API_URL}/user/updateuser`, user, { headers });
};
const findUser = id => {
  let headers = { Authorization: store.state.user_token };
  return axios.get(`${process.env.API_URL}/user/finduser/${id}`, { headers });
};
const addUser = user => {
  let headers = { Authorization: store.state.user_token };
  return axios.post(`${process.env.API_URL}/user/adduser`, user, { headers });
};
const deleteUser = id => {
  let headers = { Authorization: store.state.user_token };
  return axios.delete(`${process.env.API_URL}/user/deleteuser/${id}`, {
    headers
  });
};
export { allUser, updateUser, findUser, addUser, deleteUser, urllogin, urlregister };
