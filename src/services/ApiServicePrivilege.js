import axios from "axios";
import {store} from '../store/store'

const getAllPrivilege = () => axios.get(`${process.env.API_URL}/privilegios/allprivilegios`,{headers:{Authorization:store.state.user_token}});


const findPrivilege = (id) => axios.get(`${process.env.API_URL}/privilegios/findprivilege/${id}`,{headers:{Authorization:store.state.user_token}});


const updatePrivilege = (privilege) => axios.put(`${process.env.API_URL}/privilegios/updateprivilegio`,privilege,{headers:{Authorization:store.state.user_token}});

const addPrivilege = privilege => axios.post(`${process.env.API_URL}/privilegios/addprivilegio`, privilege,{headers:{Authorization:store.state.user_token}});

export {getAllPrivilege, findPrivilege, updatePrivilege, addPrivilege};
