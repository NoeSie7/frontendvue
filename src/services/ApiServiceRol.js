import axios from "axios";
import {store} from '../store/store';
const getAllRoles = () => axios.get(`${process.env.API_URL}/roles/allroles`,{headers:{Authorization:store.state.user_token}});

const findRol = id => axios.get(`${process.env.API_URL}/roles/findrol/${id}`, {headers:{Authorization:store.state.user_token}});

const updateRol = rol => axios.put(`${process.env.API_URL}/roles/updaterol`, rol, {headers:{Authorization:store.state.user_token}});

const addRol = rol => axios.post(`${process.env.API_URL}/roles/addrol`, rol, {headers:{Authorization:store.state.user_token}});

const deleteRol = id => axios.delete(`${process.env.API_URL}/roles/deleterol/${id}`, {headers:{Authorization:store.state.user_token}});

export { getAllRoles, updateRol, findRol, addRol, deleteRol };
