import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    user_token: null,
    user_name: null,
    user_email: null,
    user_password: null,
  },
  mutations: {
       setToken(state, tok) {
         state.user_token = tok;
       },
       setUserEmailStore(state, email) {
         state.user_email = email;
       },
       setUserNameStore(state, name) {
        state.user_name = name;
      },
       setUserPasswordStore(state, pass) {
          state.user_password = pass;
       },
  }
});
