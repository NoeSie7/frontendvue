// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import {router} from './routes/router';
import './language/ml';
import FlagIcon from 'vue-flag-icon';
import {store} from "./store/store"
import BootstrapVue from "bootstrap-vue";

Vue.use(BootstrapVue);
 Vue.use(FlagIcon);
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router: router,
  store: store,
  render: h => h(App)
})
