import Vue from 'vue';
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage';

Vue.use(MLInstaller)

export default new MLCreate({
  initial: 'Español',
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('English').create({
      usuario: 'User:',
      anadir: 'Add',
      contrasena: 'Password:',
      confirme_contrasena: 'Confirm password',
      eliminar: 'Delete',
      editar_roles: 'Edit Roles',
      roles: 'Roles:',
      privilegios: 'Privileges:',
      nombre: 'Name:',
      email: 'Email:',
      actualizar: 'Update',
      actualizar_rol: 'Update Rol',
      atras: "Go Back"
    }),
    new MLanguage('Portugues').create({
      usuario: 'Usuário:',
      anadir: 'Adicionar',
      eliminar: 'Eliminar',
      contrasena: 'Senha',
      confirme_contrasena: 'Confirmar senha',
      editar_roles: 'Edite Funções',
      roles: 'Funções:',
      privilegios: 'Privilégios:',
      nombre: 'Nome:',
      email: 'Email:',
      actualizar: 'Atualização',
      actualizar_rol: 'Atualização Funções',
      atras: 'Voltar Atrás'

    }),
    new MLanguage('Español').create({
      usuario: 'Usuario:',
      anadir: 'Añadir',
      eliminar: 'Eliminar',
      contrasena: 'Contraseña:',
      confirme_contrasena: 'Confirme Contraseña',
      editar_roles: 'Editar Roles',
      roles: 'Roles:',
      privilegios: 'Privilegios:',
      nombre: 'Nombre:',
      email: 'Email:',
      actualizar: 'Actualizar',
      actualizar_rol: 'Actualizar Roles',
      atras: 'Volver Atrás'
    }),
  ]
})
